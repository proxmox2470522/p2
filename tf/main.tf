terraform {
  required_providers {
    proxmox = {
      source  = "telmate/proxmox"
      version = "2.9.11"
    }
  }
  required_version = ">= 0.13"
}

provider "proxmox" {
  pm_api_url          = "https://pve1:8006/api2/json"
  pm_api_token_id     = "example@pam!new_token_id"
  pm_api_token_secret = "8cd645fe-3ffd-4ebd-b3e0-ca8eb08c5800"
  pm_tls_insecure     = true
}

resource "proxmox_lxc" "basic" {
  target_node  = "pve1"
  hostname     = "lxc-basic"
  ostemplate   = "local:vztmpl/debian-12-standard_12.0-1_amd64.tar.zst"
#  password     = "123456"
  start        = true
  unprivileged = true
  ssh_public_keys = <<-EOT
    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDkc1WyLK5cNcyOJqyViH/iRxol1kH+j8v/gfuKAobw8kx+KRW1u6ow4TUIOEYRzQ4XfW3QQq9tWJUlUxMF9legWXOlmdlititpZ5MHtY9tprzy0+sudUgk6kwFwxBZdrZOzE5r4KjdYM9zGvW9PvtTYvhYTu6x/jwFLZ0luRtJNYvynXD70kczMqrCsFhxdkYEbqwum2V3JhB8LIUPGo63MRnkWeJ2KShNa6Aq2CMKFzX8/+wus2irXis9gZ0t8s4KaAH9n8dZBmXB4DgdEWifhvvO9g22C/00aZ6I9XWqN1Z14lpphEBEsoCBY8+n6qoSDjhEG2tqeZX2n+XEZ8qx root@localhost
  EOT
  features {
    nesting = true
  }
  // Terraform will crash without rootfs defined
  rootfs {
    storage = "local-lvm"
    size    = "1G"
  }
  network {
    name   = "eth0"
    bridge = "vmbr0"
    ip     = "10.2.2.146/24"
    gw     = "10.2.2.1"
    ip6    = "auto"
  }
}
